\chapter{Transition Time Estimation ``Proof''}\label{app:tte}

First and foremost, this is not a complete proof of the method, this only proves two things:

\begin{itemize}
    \item \(E[n_{0}] > 0\), \(E[n_{f}] > 0\) asymptotically.
    \item \(E[n_{t}] = 0\).
\end{itemize}

Where \(E[n]\) is any metric of classification error (MSE, categorical cross entropy, etc) at iteration \(n\). \(n_{t}\), \(n_{0}\) and \(n_{f}\) are the transition, initial and final iteration of the algorithm. For all intended purposes, the two distributions have no overlap in all dimensions, that is, they are LS.

By definition, a PLA has \(0\) classification error if the data presented to it is LS. With this, we have by definition:

\begin{equation}
    E[n_{t}] = 0
\end{equation}

Because when \(n = n_{t}\), the window has \(L/2\) samples from each class, and by feeding it to a PLA, the error is always \(0\). However, when the window has only samples from one class, there is still a chance that the error is \(0\), given that the points happened to spread themselves around a line. Therefore the first objective is to prove:


\begin{lemma}
    Given a limited distribution \(f\), the probability that two subsets of \(N/2\) points taken from the \(f\) are linearly separable is bounded by \(P = 1/2^{N-1}\).
\end{lemma}

\begin{proof}
    The only two binary sets capable of being separated by a line would be \(N/2\) zeros followed by \(N/2\) ones, or vice-versa. This gives a probability of:

    \begin{equation}
        P = 2 P_{0}^{N/2} P_{1}^{N/2},
    \end{equation}
    
    where \(P_{0}\) is the probability of being classified as Class \(0\), \(P_{1}\) is analogous. Knowing that \(P_{0} + P_{1} = 1\) one can reach:

    \begin{equation}
        P = 2 P_{0}^{N/2} (1 - P_{0})^{N/2}
    \end{equation}

    In order to find the maximum value for this equation, one derives in respect with \(P_{0}\), making \(N/2 = k\) one has:

    \begin{equation}\label{tteP:initial_derivative_prob}
        \frac{\partial P}{\partial P_{0}} = 2kP_{0}^{k-1}(1-P_{0})^{k} - 
        2kP_{0}^{k}(1-P_{0})^{k-1}
    \end{equation}
    
    Setting Equation \ref{tteP:initial_derivative_prob} equal to zero yields:

    \begin{equation}
        \begin{split}
            2 k P_{0}^{k-1}(1-P_{0})^{k} &= 2 k P_{0}^{k}(1-P_{0})^{k-1} \\
            P_{0}^{-1} &= (1-P_{0})^{-1} \\
            P_{0} &= 1-P_{0} \\
            P_{0} &= \frac{1}{2}
        \end{split}
    \end{equation}

    Therefore, the maximum probability, \(P_{\max}\) is given when \(P_{0} = P_{1} = 1/2\), which applying to Equation yields:

    \begin{equation}
        \begin{split}
            P_{\max} &= 2 \left( \frac{1}{2} \right)^{N/2} \left( \frac{1}{2} \right)^{N/2} \\
            P_{\max} &= 2 \left( \frac{1}{2} \right)^{N} = \frac{1}{2^{N-1}}
        \end{split}
    \end{equation}

    And, by definition, \(P_{\max} > P\).
\end{proof}

Therefore, as long as the window is big enough, the probability that \(E[n] > 0\)  for all \(n\) given that:

\begin{equation}
    C[n+k] = i \quad | \quad \frac{-L}{2} \leq k \leq \frac{L}{2}, i = 0 OR i = 1
\end{equation}

which means that, as long all samples within the window's boundary are from the same binary class \(i\).

% It is also possible to achieve similar results using a non-linear separator, the circle.

% \section{Circular Separator}

% Suppose two spherical uniform distributions \(f\) and \(g\)

% \begin{equation}
%     P_{0} = \frac{A_{0}}{A_{T}} = \frac{\pi r_{0}^{2}}{\pi R^{2}} = \frac{r_{0}^{2}}{R^{2}}
% \end{equation}

% \begin{equation}
%     P_{1} = 1-P_{0} = \left(1-\frac{r_{0}^{2}}{R^{2}}\right)
% \end{equation}

% The probability that the set is LS, that is, \(E[n_{0}]\) is zero is:

% \begin{equation}
%     P(E[n_{0}] = 0) = \left(\frac{r_{0}}{R^{2}} \right)^{L} \cdot (R^{2} - r^{2}_{0})^{L/2}
% \end{equation}

% Taking the derivative with respect to \(r_{0}\) yields:

% \begin{equation}
%     \begin{split}
%         \frac{\partial P(E[n_{0}] = 0)}{\partial r_{0}} &= \frac{1}{R^{2L}} \left[Lr^{L-1}_{0} (R^{2} - r^{2}_{0})^{L/2} - \frac{L}{2} (R^{2} - r^{2}_{0})^{L/2-1} 2 r_{0} \right] \\ 
%         &= N \left[r^{L-1}_{0} (R^{2} - r^{2}_{0})^{L/2} - (R^{2} - r^{2}_{0})^{L/2-1} r_{0} \right]
%     \end{split}
% \end{equation}

% Now, to discover the best radius, that is, the one that maximizes the probability that the error is 0, one equates the aforementioned derivative with 0:

% \begin{equation}
%     \begin{split}
%         \frac{\partial P(E[n_{0}] = 0)}{\partial r_{0}} = 0 &\rightarrow r^{L-1}_{0} (R^{2} - r^{2}_{0})^{L/2} - (R^{2} - r^{2}_{0})^{L/2-1} r_{0} = 0 \\
%         &\rightarrow r^{L-1}_{0} (R^{2} - r^{2}_{0})^{L/2} = (R^{2} - r^{2}_{0})^{L/2-1} r_{0} \\
%         &\rightarrow 1 = \frac{r^{2}_{0}}{R^{2}-r^{2}_{0}} \\
%         &\rightarrow r_{0}^2 = \frac{R^{2}}{2} \rightarrow r_{0} = \frac{R}{\sqrt{2}}
%     \end{split}
% \end{equation}

% Thus giving us:

% \begin{equation}
%     P_{0} = \frac{R^{2}/2}{R^{2}} = 1/2
% \end{equation}

% This in turn gives us the same calculations for the linear case, yield then:

% \begin{equation}
%     P(E[n_{0}] = 0) = \frac{1}{2^{L-1}}
% \end{equation}

This means that, given a sufficiently large \(L\), as long as \(P/2 > L\) the probability of having no initial error is asymptotically close to 0. If one has \(L = 50\), \(P(E[n_{0}] = 0) \approx 8.9e-16\). This implies that the graph should look like a funnel (Figure \ref{fig:tte_example}). The problem is that, by the writing of this work, there is no proof that the error monotonically descends to 0. One can however, try to explain it by induction and considering how the gradient changes.

Given a model with parameters \(\theta\), one can define an update rule like:

\begin{equation}
    \theta[k] = \theta[k-1] - \eta \frac{\partial \theta}{\partial \xi_{i}},
\end{equation}

Where \(\xi_{i}\) is the error prevenient from sample \(i\). This error can be defined as the distance from the model's output and the target's label. If we suppose that the
\(\theta\) space is convex, this update rule will always reach the best \(\theta\) possible, \(\theta^{*}\). However, if we suddenly start changing correct labels to wrong labels, the gradient will change, and this update rule will make \(\theta\) move further away from \(\theta^{*}\). 

So this means that, as long as the window has a gradual increase of correct labels inside it, the error should decrease accordingly. This means that having a set of samples like 000000|001111 is better than 000000|000011. Simply because the former has more samples with the correct labelling, and the gradient will be ``more correct'' than it would when comparing with the latter case.