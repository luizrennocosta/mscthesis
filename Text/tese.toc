\babel@toc {brazil}{}
\babel@toc {english}{}
\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {chapter}{List of Figures}{x}{chapter*.3}
\contentsline {chapter}{List of Tables}{xii}{chapter*.4}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Objective}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Theory}{2}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Learning}{2}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Supervised Learning}{3}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Unsupervised Learning}{4}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}Artificial Neural Networks}{4}{section.1.4}
\contentsline {section}{\numberline {1.5}Perceptron}{6}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Multi Layer Perceptron}{7}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Backpropagation}{8}{subsection.1.5.2}
\contentsline {section}{\numberline {1.6}Training a Machine Learning Model}{13}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}\textit {k}-fold Cross-Validation}{15}{subsection.1.6.1}
\contentsline {section}{\numberline {1.7}Mechanical Properties}{16}{section.1.7}
\contentsline {section}{\numberline {1.8}Acoustic Emission}{18}{section.1.8}
\contentsline {section}{\numberline {1.9}Bibliography Review}{19}{section.1.9}
\contentsline {chapter}{\numberline {2}Materials and Methods}{21}{chapter.2}
\contentsline {section}{\numberline {2.1}Acoustic Emission Test}{21}{section.2.1}
\contentsline {section}{\numberline {2.2}Streaming Raw Data}{25}{section.2.2}
\contentsline {section}{\numberline {2.3}Preprocessing}{26}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Resolution Analysis}{27}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}TOFD Removal}{28}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Pressure Bomb Removal}{30}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Wave Capture}{31}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Estimating Noise Level}{32}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Timing Parameters}{33}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Acoustic Emission Parameters}{36}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Frequency Data}{36}{subsection.2.4.4}
\contentsline {section}{\numberline {2.5}Database Structure}{38}{section.2.5}
\contentsline {section}{\numberline {2.6}Model Definition}{38}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Network Size}{39}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Transition Time Estimation}{40}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}Input Correlation Analysis}{41}{subsection.2.6.3}
\contentsline {subsection}{\numberline {2.6.4}Relevance Analysis}{42}{subsection.2.6.4}
\contentsline {chapter}{\numberline {3}Results}{43}{chapter.3}
\contentsline {section}{\numberline {3.1}Preprocessing}{43}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Resolution Analysis}{43}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Frequency Data}{45}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Input Correlation \& Relevance Analysis}{47}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Transition-Time-Estimation}{53}{section.3.2}
\contentsline {section}{\numberline {3.3}Classification Results}{55}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}CP2}{56}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}CP3}{57}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}CP4}{59}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Conclusion}{62}{section.3.4}
\contentsline {chapter}{\numberline {4}Future Works}{63}{chapter.4}
\contentsline {chapter}{Bibliography}{64}{chapter*.72}
\contentsline {chapter}{Appendix \numberline {A}Software Documentation}{68}{Appendix.1.A}
\contentsline {chapter}{Appendix \numberline {B}Transition Time Estimation ``Proof''}{80}{Appendix.1.B}
