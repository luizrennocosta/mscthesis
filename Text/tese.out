\BOOKMARK [0][-]{chapter*.3}{List of Figures}{}% 1
\BOOKMARK [0][-]{chapter*.4}{List of Tables}{}% 2
\BOOKMARK [0][-]{chapter.1}{Introduction}{}% 3
\BOOKMARK [1][-]{section.1.1}{Introduction}{chapter.1}% 4
\BOOKMARK [1][-]{section.1.2}{Objective}{chapter.1}% 5
\BOOKMARK [1][-]{section.1.3}{Theory}{chapter.1}% 6
\BOOKMARK [2][-]{subsection.1.3.1}{Learning}{section.1.3}% 7
\BOOKMARK [2][-]{subsection.1.3.2}{Supervised Learning}{section.1.3}% 8
\BOOKMARK [2][-]{subsection.1.3.3}{Unsupervised Learning}{section.1.3}% 9
\BOOKMARK [1][-]{section.1.4}{Artificial Neural Networks}{chapter.1}% 10
\BOOKMARK [1][-]{section.1.5}{Perceptron}{chapter.1}% 11
\BOOKMARK [2][-]{subsection.1.5.1}{Multi Layer Perceptron}{section.1.5}% 12
\BOOKMARK [2][-]{subsection.1.5.2}{Backpropagation}{section.1.5}% 13
\BOOKMARK [1][-]{section.1.6}{Training a Machine Learning Model}{chapter.1}% 14
\BOOKMARK [2][-]{subsection.1.6.1}{k-fold Cross-Validation}{section.1.6}% 15
\BOOKMARK [1][-]{section.1.7}{Mechanical Properties}{chapter.1}% 16
\BOOKMARK [1][-]{section.1.8}{Acoustic Emission}{chapter.1}% 17
\BOOKMARK [1][-]{section.1.9}{Bibliography Review}{chapter.1}% 18
\BOOKMARK [0][-]{chapter.2}{Materials and Methods}{}% 19
\BOOKMARK [1][-]{section.2.1}{Acoustic Emission Test}{chapter.2}% 20
\BOOKMARK [1][-]{section.2.2}{Streaming Raw Data}{chapter.2}% 21
\BOOKMARK [1][-]{section.2.3}{Preprocessing}{chapter.2}% 22
\BOOKMARK [2][-]{subsection.2.3.1}{Resolution Analysis}{section.2.3}% 23
\BOOKMARK [2][-]{subsection.2.3.2}{TOFD Removal}{section.2.3}% 24
\BOOKMARK [2][-]{subsection.2.3.3}{Pressure Bomb Removal}{section.2.3}% 25
\BOOKMARK [1][-]{section.2.4}{Wave Capture}{chapter.2}% 26
\BOOKMARK [2][-]{subsection.2.4.1}{Estimating Noise Level}{section.2.4}% 27
\BOOKMARK [2][-]{subsection.2.4.2}{Timing Parameters}{section.2.4}% 28
\BOOKMARK [2][-]{subsection.2.4.3}{Acoustic Emission Parameters}{section.2.4}% 29
\BOOKMARK [2][-]{subsection.2.4.4}{Frequency Data}{section.2.4}% 30
\BOOKMARK [1][-]{section.2.5}{Database Structure}{chapter.2}% 31
\BOOKMARK [1][-]{section.2.6}{Model Definition}{chapter.2}% 32
\BOOKMARK [2][-]{subsection.2.6.1}{Network Size}{section.2.6}% 33
\BOOKMARK [2][-]{subsection.2.6.2}{Transition Time Estimation}{section.2.6}% 34
\BOOKMARK [2][-]{subsection.2.6.3}{Input Correlation Analysis}{section.2.6}% 35
\BOOKMARK [2][-]{subsection.2.6.4}{Relevance Analysis}{section.2.6}% 36
\BOOKMARK [0][-]{chapter.3}{Results}{}% 37
\BOOKMARK [1][-]{section.3.1}{Preprocessing}{chapter.3}% 38
\BOOKMARK [2][-]{subsection.3.1.1}{Resolution Analysis}{section.3.1}% 39
\BOOKMARK [2][-]{subsection.3.1.2}{Frequency Data}{section.3.1}% 40
\BOOKMARK [2][-]{subsection.3.1.3}{Input Correlation \046 Relevance Analysis}{section.3.1}% 41
\BOOKMARK [1][-]{section.3.2}{Transition-Time-Estimation}{chapter.3}% 42
\BOOKMARK [1][-]{section.3.3}{Classification Results}{chapter.3}% 43
\BOOKMARK [2][-]{subsection.3.3.1}{CP2}{section.3.3}% 44
\BOOKMARK [2][-]{subsection.3.3.2}{CP3}{section.3.3}% 45
\BOOKMARK [2][-]{subsection.3.3.3}{CP4}{section.3.3}% 46
\BOOKMARK [1][-]{section.3.4}{Conclusion}{chapter.3}% 47
\BOOKMARK [0][-]{chapter.4}{Future Works}{}% 48
\BOOKMARK [0][-]{chapter*.72}{Bibliography}{}% 49
\BOOKMARK [0][-]{Appendix.1.A}{Appendix Software Documentation}{}% 50
\BOOKMARK [0][-]{Appendix.1.B}{Appendix Transition Time Estimation ``Proof''}{}% 51
